﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Plugin.Connectivity;
using PUB.Models3;
using PUB.models4;
using Xamarin.Forms;

namespace PUB
{
    public partial class OtherStats : ContentPage
    {
        private const string ID_PRE = "division.bro.official.2018-0";
        public static string playerID = "";
        Dictionary<string, long> modeStats;
        SeasonData SeasonData = null;
        public OtherStats()
        {
            InitializeComponent();

			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await DisplayAlert("Warning", "Internet connection lost! Please reconnect your device while we send you to the home page!", "OK");

                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }

            };
         
			DataGrid.BackgroundColor = Color.FromRgba(0, 0, 0, .3);
			PlayerName.BackgroundColor = Color.FromRgba(0, 0, 0, .3);         
            PlayerName.Text = App.userName;
            PopulateSeasonPicker();
            PopulateModePicker();
            FindPlayer();

        }

		async void Handle_SearchClicked(object sender, System.EventArgs e)
        {
			await Navigation.PopToRootAsync();
        }


        private void PopulateSeasonPicker()
        {


            SeasonPicker.Items.Add("Season 1");
            SeasonPicker.Items.Add("Season 2");
            SeasonPicker.Items.Add("Season 3");
            SeasonPicker.Items.Add("Season 4");
            SeasonPicker.Items.Add("Season 5 - Current Season");


            SeasonPicker.SelectedIndex = 4;
        }
        private void PopulateModePicker()
        {


            ModePicker.Items.Add("Solo FPP");
            ModePicker.Items.Add("Solo TPP");
            ModePicker.Items.Add("Duo FPP");
            ModePicker.Items.Add("Duo TPP");
            ModePicker.Items.Add("Squad FPP");
            ModePicker.Items.Add("Squad TPP");


            ModePicker.SelectedIndex = 4;
        }
        void HandleSeasonChanged(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"Inside handleseason");
            if (SeasonData != null)
            {
                FindSeasonData(SeasonPicker.SelectedIndex + 1);

            }
            else
            {
                Debug.WriteLine("ERROR NULL STUFF");

            }

        }
        void HandleModeChanged(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"Inside handlemode");
            if (SeasonData != null)
            {
                setModeStats();
                displayModeData(modeStats);

            }


        }
        void setModeStats()
        {
            switch (ModePicker.SelectedIndex)
            {
                case 0:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.SoloFpp;
                    break;
                case 1:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.Solo;
                    break;
                case 2:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.DuoFpp;
                    break;
                case 3:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.Duo;
                    break;
                case 4:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.SquadFpp;
                    break;
                case 5:
                    modeStats = SeasonData.Data.Attributes.GameModeStats.Squad;
                    break;


            }
        }

        void displayModeData(Dictionary<string, long> modeStats)
        {

            if (modeStats["roundsPlayed"] != 0)
            {
                Kills.Text = modeStats["kills"].ToString();
                Deaths.Text = modeStats["losses"].ToString();
                Assists.Text = modeStats["assists"].ToString();
                Top10.Text = modeStats["top10s"].ToString();
                Suicides.Text = modeStats["suicides"].ToString();
                TeamKills.Text = modeStats["teamKills"].ToString();
                WalkDist.Text = modeStats["walkDistance"].ToString() + " km";
                KD.Text = string.Format("{0:N2}", (modeStats["kills"] / (double)modeStats["losses"]));
                Days.Text = modeStats["days"].ToString();
                Games.Text = modeStats["roundsPlayed"].ToString();
                Wins.Text = modeStats["wins"].ToString();
                WinRate.Text = string.Format("{0:N2}%", (modeStats["wins"] / (double)modeStats["roundsPlayed"]));
                MostKills.Text = modeStats["roundMostKills"].ToString();
                LongestKill.Text = modeStats["longestKill"].ToString() + " m";
            }
            else
            {

                DisplayAlert("No Games Played.", "No games played in this mode", "OK");
                Kills.Text = "N/A";
                Deaths.Text = "N/A";
                Assists.Text = "N/A";
                Top10.Text = "N/A";
                Suicides.Text = "N/A";
                TeamKills.Text = "N/A";
                WalkDist.Text = "N/A";
                KD.Text = "N/A";
                Days.Text = "N/A";
                Games.Text = "N/A";
                Wins.Text = modeStats["wins"].ToString();
                WinRate.Text = "N/A";
                MostKills.Text = "N/A";
                LongestKill.Text = "N/A";

            }

        }


        // "https://api.playbattlegrounds.com/shards/pc-na/players/TrulySlothful/seasons/division.bro.official.2018-05"
        async void FindSeasonData(int seasonNum)
        {
			if (CrossConnectivity.Current.IsConnected)
			{



				Debug.WriteLine($"Inside findseason");
				HttpClient client = new HttpClient();

				string sID = ID_PRE + seasonNum;
				var uri = new Uri(
					string.Format(
						$"https://api.playbattlegrounds.com/shards/pc-na/players/{playerID}/seasons/{ID_PRE}{seasonNum}"));
				Debug.WriteLine($"This is the call: https://api.playbattlegrounds.com/shards/pc-na/players/{playerID}/seasons/{ID_PRE}{seasonNum}");
				var request = new HttpRequestMessage();
				request.Method = HttpMethod.Get;
				request.RequestUri = uri;
				//request.Headers.Add("Application", "application / json");
				request.Headers.Add("Accept", "application/vnd.api+json");
				request.Headers.Add("Authorization", $"Bearer {Keys.PersonalKey}");

				HttpResponseMessage response = await client.SendAsync(request);
				SeasonData = null;
				modeStats = null;

				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					Debug.WriteLine($"{content.ToString()}");
					SeasonData = SeasonData.FromJson(content);
					setModeStats();

					displayModeData(modeStats);

					// long value  = ApiData.Data.Attributes.GameModeStats.DuoFpp["kills"];

					//.WriteLine($"Kills for {App.userName} in DUOS {value} for season {seasonNum}");
					Debug.WriteLine("Success");


				}
				else
				{
					Debug.WriteLine($"Response for failure: {response.ToString()}");
				}
			}
        }

        async void FindPlayer()
        {
			if (CrossConnectivity.Current.IsConnected)
			{
				Debug.WriteLine($"Inside findplayer");

				HttpClient client = new HttpClient();


				var uri = new Uri(
					string.Format(
						$"https://api.playbattlegrounds.com/shards/pc-na/players?filter[playerNames]={App.userName}"));

				var request = new HttpRequestMessage();
				request.Method = HttpMethod.Get;
				request.RequestUri = uri;
				//request.Headers.Add("Application", "application / json");
				request.Headers.Add("Accept", "application/vnd.api+json");
				request.Headers.Add("Authorization", $"Bearer {Keys.PersonalKey}");

				HttpResponseMessage response = await client.SendAsync(request);
				PlayerInfo ApiData = null;
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					Debug.WriteLine($"{content.ToString()}");
					ApiData = PlayerInfo.FromJson(content);
					playerID = ApiData.Data[0].Id;

					Debug.WriteLine($"Player id for {App.userName} was {playerID}");
					Debug.WriteLine("Success");
					FindSeasonData(5);

				}
				else
				{
					Debug.WriteLine("API Request failure");
				}
			}
        }
    }
}
