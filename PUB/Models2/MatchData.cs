﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PUB.Models;
//
//    var matchData = MatchData.FromJson(jsonString);

namespace PUB.Models2
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class MatchData
    {
        [JsonProperty("data")]
        public Data Data { get; set; }

        [JsonProperty("included")]
        public Included[] Included { get; set; }

        [JsonProperty("links")]
        public MatchDataLinks Links { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("attributes")]
        public DataAttributes Attributes { get; set; }

        [JsonProperty("relationships")]
        public DataRelationships Relationships { get; set; }

        [JsonProperty("links")]
        public DataLinks Links { get; set; }
    }

    public partial class DataAttributes
    {
        [JsonProperty("mapName")]
        public string MapName { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("stats")]
        public object Stats { get; set; }

        [JsonProperty("gameMode")]
        public string GameMode { get; set; }

        [JsonProperty("titleId")]
        public string TitleId { get; set; }

        [JsonProperty("shardId")]
        public string ShardId { get; set; }

        [JsonProperty("tags")]
        public object Tags { get; set; }
    }

    public partial class DataLinks
    {
        [JsonProperty("self")]
        public string Self { get; set; }

        [JsonProperty("schema")]
        public string Schema { get; set; }
    }

    public partial class DataRelationships
    {
        [JsonProperty("assets")]
        public Assets Assets { get; set; }

        [JsonProperty("rosters")]
        public Assets Rosters { get; set; }
    }

    public partial class Assets
    {
        [JsonProperty("data")]
        public Datum[] Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class Included
    {
        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("attributes")]
        public IncludedAttributes Attributes { get; set; }

        [JsonProperty("relationships", NullValueHandling = NullValueHandling.Ignore)]
        public IncludedRelationships Relationships { get; set; }
    }

    public partial class IncludedAttributes
    {
        [JsonProperty("stats", NullValueHandling = NullValueHandling.Ignore)]
        public Stats Stats { get; set; }

        [JsonProperty("actor", NullValueHandling = NullValueHandling.Ignore)]
        public Actor? Actor { get; set; }

        /*[JsonProperty("shardId", NullValueHandling = NullValueHandling.Ignore)]
        public ShardId? ShardId { get; set; }
        */
        [JsonProperty("won", NullValueHandling = NullValueHandling.Ignore)]
        public Won? Won { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("createdAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("URL", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }
    }

    public partial class Stats
    {
        [JsonProperty("DBNOs", NullValueHandling = NullValueHandling.Ignore)]
        public long? DbnOs { get; set; }

        [JsonProperty("assists", NullValueHandling = NullValueHandling.Ignore)]
        public long? Assists { get; set; }

        [JsonProperty("boosts", NullValueHandling = NullValueHandling.Ignore)]
        public long? Boosts { get; set; }

        [JsonProperty("damageDealt", NullValueHandling = NullValueHandling.Ignore)]
        public double? DamageDealt { get; set; }

        //[JsonProperty("deathType", NullValueHandling = NullValueHandling.Ignore)]
        //public DeathType? DeathType { get; set; }

        [JsonProperty("headshotKills", NullValueHandling = NullValueHandling.Ignore)]
        public long? HeadshotKills { get; set; }

        [JsonProperty("heals", NullValueHandling = NullValueHandling.Ignore)]
        public long? Heals { get; set; }

        [JsonProperty("killPlace", NullValueHandling = NullValueHandling.Ignore)]
        public long? KillPlace { get; set; }

        [JsonProperty("killPoints", NullValueHandling = NullValueHandling.Ignore)]
        public long? KillPoints { get; set; }

        [JsonProperty("killPointsDelta", NullValueHandling = NullValueHandling.Ignore)]
        public double? KillPointsDelta { get; set; }

        [JsonProperty("killStreaks", NullValueHandling = NullValueHandling.Ignore)]
        public long? KillStreaks { get; set; }

        [JsonProperty("kills", NullValueHandling = NullValueHandling.Ignore)]
        public long? Kills { get; set; }

        [JsonProperty("lastKillPoints", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastKillPoints { get; set; }

        [JsonProperty("lastWinPoints", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastWinPoints { get; set; }

        [JsonProperty("longestKill", NullValueHandling = NullValueHandling.Ignore)]
        public long? LongestKill { get; set; }

        [JsonProperty("mostDamage", NullValueHandling = NullValueHandling.Ignore)]
        public long? MostDamage { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("playerId", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerId { get; set; }

        [JsonProperty("revives", NullValueHandling = NullValueHandling.Ignore)]
        public long? Revives { get; set; }

        [JsonProperty("rideDistance", NullValueHandling = NullValueHandling.Ignore)]
        public double? RideDistance { get; set; }

        [JsonProperty("roadKills", NullValueHandling = NullValueHandling.Ignore)]
        public long? RoadKills { get; set; }

        [JsonProperty("teamKills", NullValueHandling = NullValueHandling.Ignore)]
        public long? TeamKills { get; set; }

        [JsonProperty("timeSurvived", NullValueHandling = NullValueHandling.Ignore)]
        public long? TimeSurvived { get; set; }

        [JsonProperty("vehicleDestroys", NullValueHandling = NullValueHandling.Ignore)]
        public long? VehicleDestroys { get; set; }

        [JsonProperty("walkDistance", NullValueHandling = NullValueHandling.Ignore)]
        public double? WalkDistance { get; set; }

        [JsonProperty("weaponsAcquired", NullValueHandling = NullValueHandling.Ignore)]
        public long? WeaponsAcquired { get; set; }

        [JsonProperty("winPlace", NullValueHandling = NullValueHandling.Ignore)]
        public long? WinPlace { get; set; }

        [JsonProperty("winPoints", NullValueHandling = NullValueHandling.Ignore)]
        public long? WinPoints { get; set; }

        [JsonProperty("winPointsDelta", NullValueHandling = NullValueHandling.Ignore)]
        public double? WinPointsDelta { get; set; }

        [JsonProperty("rank", NullValueHandling = NullValueHandling.Ignore)]
        public long? Rank { get; set; }

        [JsonProperty("teamId", NullValueHandling = NullValueHandling.Ignore)]
        public long? TeamId { get; set; }
    }

    public partial class IncludedRelationships
    {
        [JsonProperty("team")]
        public Assets Team { get; set; }

        [JsonProperty("participants")]
        public Assets Participants { get; set; }
    }

    public partial class MatchDataLinks
    {
        [JsonProperty("self")]
        public string Self { get; set; }
    }

    public partial class Meta
    {
    }

    public enum TypeEnum { Asset, Participant, Roster };

    public enum Actor { Empty };



    //public enum ShardId { PcNa };

    //public enum DeathType { Alive, Byplayer };

    public enum Won { False, True };

    public partial class MatchData
    {
        public static MatchData FromJson(string json) => JsonConvert.DeserializeObject<MatchData>(json, PUB.Models.Converter.Settings);
    }

    static class TypeEnumExtensions
    {
        public static TypeEnum? ValueForString(string str)
        {
            switch (str)
            {
                case "asset": return TypeEnum.Asset;
                case "participant": return TypeEnum.Participant;
                case "roster": return TypeEnum.Roster;
                default: return null;
            }
        }

        public static TypeEnum ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this TypeEnum value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case TypeEnum.Asset: serializer.Serialize(writer, "asset"); break;
                case TypeEnum.Participant: serializer.Serialize(writer, "participant"); break;
                case TypeEnum.Roster: serializer.Serialize(writer, "roster"); break;
            }
        }
    }

    static class ActorExtensions
    {
        public static Actor? ValueForString(string str)
        {
            switch (str)
            {
                case "": return Actor.Empty;
                default: return null;
            }
        }

        public static Actor ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this Actor value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case Actor.Empty: serializer.Serialize(writer, ""); break;
            }
        }
    }
    /*
    static class ShardIdExtensions
    {
           
        
        public static ShardId? ValueForString(string str)
        {
            switch (str)
            {
                case "pc-na": return ShardId.PcNa;
                default: return null;
            }
        }

        public static ShardId ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this ShardId value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case ShardId.PcNa: serializer.Serialize(writer, "pc-na"); break;
            }
        }
    }
*//*
    static class DeathTypeExtensions
    {
        public static DeathType? ValueForString(string str)
        {
            switch (str)
            {
                case "alive": return DeathType.Alive;
                case "byplayer": return DeathType.Byplayer;
                default: return null;
            }
        }

        public static DeathType ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this DeathType value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case DeathType.Alive: serializer.Serialize(writer, "alive"); break;
                case DeathType.Byplayer: serializer.Serialize(writer, "byplayer"); break;
            }
        }
    }*/

    static class WonExtensions
    {
        public static Won? ValueForString(string str)
        {
            switch (str)
            {
                case "false": return Won.False;
                case "true": return Won.True;
                default: return null;
            }
        }

        public static Won ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this Won value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case Won.False: serializer.Serialize(writer, "false"); break;
                case Won.True: serializer.Serialize(writer, "true"); break;
            }
        }
    }

    public static class Serialize
    {
        public static string ToJson(this MatchData self) => JsonConvert.SerializeObject(self, PUB.Models.Converter.Settings);
    }

    internal class Converter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(Actor) ||  t == typeof(Won) || t == typeof(TypeEnum?) || t == typeof(Actor?)  || t == typeof(Won?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(TypeEnum))
                return TypeEnumExtensions.ReadJson(reader, serializer);
            if (t == typeof(Actor))
                return ActorExtensions.ReadJson(reader, serializer);
           /* if (t == typeof(ShardId))
                return ShardIdExtensions.ReadJson(reader, serializer);*/
            //if (t == typeof(DeathType))
              //  return DeathTypeExtensions.ReadJson(reader, serializer);
            if (t == typeof(Won))
                return WonExtensions.ReadJson(reader, serializer);
            if (t == typeof(TypeEnum?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return TypeEnumExtensions.ReadJson(reader, serializer);
            }
            if (t == typeof(Actor?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return ActorExtensions.ReadJson(reader, serializer);
            }
           /* if (t == typeof(ShardId?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return ShardIdExtensions.ReadJson(reader, serializer);
            }*/
           /* if (t == typeof(DeathType?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return DeathTypeExtensions.ReadJson(reader, serializer);
            }*/
            if (t == typeof(Won?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return WonExtensions.ReadJson(reader, serializer);
            }
            throw new Exception("Unknown type");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
            if (t == typeof(TypeEnum))
            {
                ((TypeEnum)value).WriteJson(writer, serializer);
                return;
            }
            if (t == typeof(Actor))
            {
                ((Actor)value).WriteJson(writer, serializer);
                return;
            }
           /* if (t == typeof(ShardId))
            {
                ((ShardId)value).WriteJson(writer, serializer);
                return;
            }*/
           /* if (t == typeof(DeathType))
            {
                ((DeathType)value).WriteJson(writer, serializer);
                return;
            }*/
            if (t == typeof(Won))
            {
                ((Won)value).WriteJson(writer, serializer);
                return;
            }
            throw new Exception("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new Converter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}