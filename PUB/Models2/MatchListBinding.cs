﻿using System;

using Xamarin.Forms;

namespace PUB.Models2
{
    public class MatchListBinding : ContentPage
    {
		      
		public string MatchTypeLabel
        {
            get;
            set;
        }

        public string MatchMapLabel
        {
            get;
            set;
        }

        public string MatchDate
        {
            get;
            set;
        }

        public string MatchTime
        {
            get;
            set;
        }

        public string PlaceLabel
        {
            get;
            set;
        }

        public string WonGame
        {
            get;
            set;
        }

        public string KillPlace
        {
            get;
            set;
        }

        public string KillsLabel
        {
            get;
            set;
        }

        public string AssistsLabel
        {
            get;
            set;
        }

        public string DamageLabel
        {
            get;
            set;
        }

        public string DBNO
        {
            get;
            set;
        }

        public string HeadshotKills
        {
            get;
            set;
        }

        public string LongestKill
        {
            get;
            set;
        }


        public string Heals
        {
            get;
            set;
        }

        public string Revives
        {
            get;
            set;
        }

        public string Boosts
        {
            get;
            set;
        }

        public string WalkDistance
        {
            get;
            set;
        }

        public string RideDistance
        {
            get;
            set;
        }

        public string Name1
        {
            get;
            set;
        }

        public string Name2
        {
            get;
            set;
        }

        public string Name3
        {
            get;
            set;
        }

        public string Name4
        {
            get;
            set;
        }




    }
}

