﻿using System;

using Xamarin.Forms;

namespace PUB.Models3
{
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using QuickType;
    //
    //    var welcome = Welcome.FromJson(jsonString);


        using System;
        using System.Collections.Generic;

        using System.Globalization;
        using Newtonsoft.Json;
        using Newtonsoft.Json.Converters;

        public partial class Season
        {
            [JsonProperty("data")]
            public Datum[] Data { get; set; }

            [JsonProperty("links")]
            public Links Links { get; set; }

            [JsonProperty("meta")]
            public Meta Meta { get; set; }
        }

        public partial class Datum
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("attributes")]
            public Attributes Attributes { get; set; }
        }

        public partial class Attributes
        {
            [JsonProperty("isCurrentSeason")]
            public bool IsCurrentSeason { get; set; }

            [JsonProperty("isOffseason")]
            public bool IsOffseason { get; set; }
        }

        public partial class Links
        {
            [JsonProperty("self")]
            public string Self { get; set; }
        }

        public partial class Meta
        {
        }

    public partial class Season
        {
        public static Season FromJson(string json) => JsonConvert.DeserializeObject<Season>(json, PUB.Models3.Converter.Settings);
        }

        public static class Serialize
        {
        public static string ToJson(this Season self) => JsonConvert.SerializeObject(self, PUB.Models3.Converter.Settings);
        }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

}

