﻿using System;
namespace PUB.Models3
{
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using QuickType;
    //
    //    var seasonData = SeasonData.FromJson(jsonString);

   
        using System;
        using System.Collections.Generic;

        using System.Globalization;
        using Newtonsoft.Json;
        using Newtonsoft.Json.Converters;

        public partial class SeasonData
        {
            [JsonProperty("data")]
            public Data Data { get; set; }

            [JsonProperty("links")]
            public Links Links { get; set; }

            [JsonProperty("meta")]
            public Meta Meta { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("attributes")]
            public Attributes Attributes { get; set; }

            [JsonProperty("relationships")]
            public Relationships Relationships { get; set; }
        }

        public partial class Attributes
        {
            [JsonProperty("gameModeStats")]
            public GameModeStats GameModeStats { get; set; }
        }

        public partial class GameModeStats
        {
            [JsonProperty("duo")]
            public Dictionary<string, long> Duo { get; set; }

            [JsonProperty("duo-fpp")]
            public Dictionary<string, long> DuoFpp { get; set; }

            [JsonProperty("solo")]
            public Dictionary<string, long> Solo { get; set; }

            [JsonProperty("solo-fpp")]
            public Dictionary<string, long> SoloFpp { get; set; }

            [JsonProperty("squad")]
            public Dictionary<string, long> Squad { get; set; }

            [JsonProperty("squad-fpp")]
            public Dictionary<string, long> SquadFpp { get; set; }
        }

        public partial class Relationships
        {
            [JsonProperty("player")]
            public Player Player { get; set; }

            [JsonProperty("matchesSolo")]
            public Matches MatchesSolo { get; set; }

            [JsonProperty("matchesSoloFPP")]
            public Matches MatchesSoloFpp { get; set; }

            [JsonProperty("matchesDuo")]
            public Matches MatchesDuo { get; set; }

            [JsonProperty("matchesDuoFPP")]
            public Matches MatchesDuoFpp { get; set; }

            [JsonProperty("matchesSquad")]
            public Matches MatchesSquad { get; set; }

            [JsonProperty("matchesSquadFPP")]
            public Matches MatchesSquadFpp { get; set; }

            [JsonProperty("season")]
            public Player Season { get; set; }
        }

        public partial class Matches
        {
            [JsonProperty("data")]
            public Dat[] Data { get; set; }
        }

        public partial class Dat
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("id")]
            public string Id { get; set; }
        }

        public partial class Player
        {
            [JsonProperty("data")]
            public Dat Data { get; set; }
        }

        public partial class Links
        {
            [JsonProperty("self")]
            public string Self { get; set; }
        }

        public partial class Meta
        {
        }

        public partial class SeasonData
        {
            public static SeasonData FromJson(string json) => JsonConvert.DeserializeObject<SeasonData>(json, PUB.Models3.Converter.Settings);
        }

        public static class Serialize
        {
            public static string ToJson(this SeasonData self) => JsonConvert.SerializeObject(self, PUB.Models3.Converter.Settings);
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }


}
