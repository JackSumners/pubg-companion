﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Plugin.Connectivity;
using PUB.Models;
using Xamarin.Forms;

namespace PUB
{
    public partial class PlayerStats : ContentPage
    {

        public static string playerID = "";

		List<String> matchID = new List<String>();
        public PlayerStats()
        {
            
            InitializeComponent(); 
			PlayerGrid.BackgroundColor = Color.FromRgba(0, 0, 0, .3);
			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await DisplayAlert("Warning", "Internet connection lost! Please reconnect your device while we send you to the home page!", "OK");

                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }
                
            };


            FindPlayer();
        }

        void HandleMatch_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Matches(matchID));
        }

        async void Handle_SearchClicked(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }
        
       async void FindPlayer()
        {

			if (CrossConnectivity.Current.IsConnected)
			{
				HttpClient client = new HttpClient();

				var uri = new Uri(
					string.Format(
						$"https://api.playbattlegrounds.com/shards/pc-na/players?filter[playerNames]={App.userName}"));

				var request = new HttpRequestMessage();
				request.Method = HttpMethod.Get;
				request.RequestUri = uri;
				//request.Headers.Add("Application", "application / json");
				request.Headers.Add("Accept", "application/vnd.api+json");
				request.Headers.Add("Authorization", $"Bearer {Keys.PersonalKey}");
				//client.DefaultRequestHeaders.Authorization =
				//new System.Net.Http.Headers.AuthenticationHeaderValue("Authorization", $"Bearer {Keys.PersonalKey}");
				//client.DefaultRequestHeaders.TryAddWithoutValidation("TRN-Api-Key",Keys.PersonalKey );

				HttpResponseMessage response = await client.SendAsync(request);
				StatsCereal ApiData = null;
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					ApiData = StatsCereal.FromJson(content);
					frontLabel.Text = $"Welcome: {ApiData.Data[0].Attributes.Name}";

					/*label2.Text = $"Title ID: {ApiData.Data[0].Attributes.TitleId}";
					label3.Text = $"Links Self: {ApiData.Links.Self}";
					label4.Text = $"Shard ID: {ApiData.Data[0].Attributes.ShardId}";

					label5.Text = $"Created at: {ApiData.Data[0].Attributes.CreatedAt}";
					label6.Text = $"Type: {ApiData.Data[0].Type}";
					label7.Text = $"Matches ID: {ApiData.Data[0].Relationships.Matches.Data[0].Id}";
					label8.Text = $"Match Type: {ApiData.Data[0].Relationships.Matches.Data[0].Type}";
					*/
					matchID.Add($"{ApiData.Data[0].Relationships.Matches.Data[0].Id}");
					matchID.Add($"{ApiData.Data[0].Relationships.Matches.Data[1].Id}");
					matchID.Add($"{ApiData.Data[0].Relationships.Matches.Data[2].Id}");
					matchID.Add($"{ApiData.Data[0].Relationships.Matches.Data[3].Id}");
					matchID.Add($"{ApiData.Data[0].Relationships.Matches.Data[4].Id}");

					/*for (int i = 0; i < 10; i++)
					{
						Debug.WriteLine($"{ApiData.Data[0].Relationships.Matches.Data[i].Id}");
					}*/
				}
				else
				{
					await DisplayAlert("User Not Found!", "Let's try another search!", "OK");
					await Navigation.PopToRootAsync();
				}
			}
        }
    }
}
