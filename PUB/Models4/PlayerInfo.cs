﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var player = Player.FromJson(jsonString);

namespace PUB.models4
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    public partial class PlayerInfo
    {
        [JsonProperty("data")]
        public PlayerInfoDatum[] Data { get; set; }

        [JsonProperty("links")]
        public PlayerInfoLinks Links { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }
    }

    public partial class PlayerInfoDatum
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }

        [JsonProperty("relationships")]
        public Relationships Relationships { get; set; }

        [JsonProperty("links")]
        public DatumLinks Links { get; set; }
    }

    public partial class Attributes
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("shardId")]
        public string ShardId { get; set; }

        [JsonProperty("stats")]
        public Meta Stats { get; set; }

        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }

        [JsonProperty("patchVersion")]
        public string PatchVersion { get; set; }

        [JsonProperty("titleId")]
        public string TitleId { get; set; }
    }

    public partial class Meta
    {
    }

    public partial class DatumLinks
    {
        [JsonProperty("schema")]
        public string Schema { get; set; }

        [JsonProperty("self")]
        public string Self { get; set; }
    }

    public partial class Relationships
    {
        [JsonProperty("assets")]
        public Assets Assets { get; set; }

        [JsonProperty("matches")]
        public Assets Matches { get; set; }
    }

    public partial class Assets
    {
        [JsonProperty("data")]
        public AssetsDatum[] Data { get; set; }
    }

    public partial class AssetsDatum
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class PlayerInfoLinks
    {
        [JsonProperty("self")]
        public string Self { get; set; }
    }

    public partial class PlayerInfo
    {
        public static PlayerInfo FromJson(string json) => JsonConvert.DeserializeObject<PlayerInfo>(json, PUB.models4.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this PlayerInfo self) => JsonConvert.SerializeObject(self, PUB.models4.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

