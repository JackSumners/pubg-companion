﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Plugin.Connectivity;
using PUB.Models2;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PUB
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class Matches : ContentPage
    {


        ObservableCollection<MatchListBinding> MatchInfo = new ObservableCollection<MatchListBinding>();


        public Matches(List<String> matchID)
        {
            InitializeComponent();


			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await DisplayAlert("Warning", "Internet connection lost! Please reconnect your device while we send you to the home page!", "OK");

                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }
            };

			var searchItem = new ToolbarItem()
			{
				Text = "New Search",
				Command = new Command(() => Navigation.PopToRootAsync()),
			};
			ToolbarItems.Add(searchItem);


            FindMatch(matchID);
        }

        async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listItem = (ListView)sender;
            var matchListData = (MatchListBinding)listItem.SelectedItem;
			await Navigation.PushAsync(new MoreMatchData(matchListData));
        }

        async void FindMatch(List<String> matchID)
        {

            for (int l = 0; l < 5; l++)
            {
                HttpClient client = new HttpClient();

                var uri = new Uri(
                    string.Format(
                            $"https://api.playbattlegrounds.com/shards/pc-na/matches/{matchID[l]}"));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;

                request.Headers.Add("Accept", "application/vnd.api+json");
                request.Headers.Add("Authorization", $"Bearer {Keys.PersonalKey}");


                HttpResponseMessage response = await client.SendAsync(request);
                MatchData ApiData = null;
                if (response.IsSuccessStatusCode)
                {


                    //https://app.quicktype.io?share=GbWUYZxhyA3wzwa5UJlI link quicktype all data

                    var content = await response.Content.ReadAsStringAsync();
                    ApiData = MatchData.FromJson(content);

                    int i;
                    for (i = 0; App.userName != ApiData?.Included[i]?.Attributes?.Stats?.Name; i++)
                    {
                        //Debug.WriteLine($"{ApiData?.Included[i]?.Attributes?.Stats?.Name}");               
                    }

                    double? tempDistance = ApiData?.Included[i]?.Attributes?.Stats?.WalkDistance;
                    int newDistance = Convert.ToInt32(tempDistance);

					double? tempRideDistance = ApiData?.Included[i]?.Attributes?.Stats?.RideDistance;
                    int newRideDistance = Convert.ToInt32(tempRideDistance);


                    //Converts time in seconds to a formatted string
                    string formattedTime;
                    long? tempTime = ApiData.Included[i].Attributes.Stats.TimeSurvived;
                    double minuiteT = Convert.ToDouble(tempTime / 60.00);
                    double modT = Convert.ToDouble(minuiteT % 1);
                    int min = Convert.ToInt32(minuiteT);
                    int sec = Convert.ToInt32(modT * 60);
                    if (min < 10)
                    { formattedTime = $"0{min}:{sec}"; }
                    else
                    { formattedTime = $"{min}:{sec}"; }

					string newMatchName;
					if (ApiData.Data.Attributes.MapName == "Desert_Main")
					{ newMatchName = "Miramar"; }
					else
					{ newMatchName = "Erangel"; }

					/*
					string newMatchType;
					if (ApiData.Data.Attributes.GameMode == "solo")
                    { newMatchName = "Miramar"; }
                    else
                    { newMatchName = "Erangel"; }
                    */

                    MatchListView.BackgroundColor = Color.FromRgba(0, 0, 0, .4);

                    var newList = new MatchListBinding
                    {
                        PlaceLabel = $"#{ApiData?.Included[i]?.Attributes?.Stats?.WinPlace}",
						MatchMapLabel = $"{newMatchName}",
                        MatchTime = $"{formattedTime}",
                        MatchTypeLabel = $"{ApiData.Data.Attributes.GameMode}",
                        KillsLabel = $"{ApiData?.Included[i]?.Attributes?.Stats?.Kills}",
                        AssistsLabel = $"{ApiData?.Included[i]?.Attributes?.Stats?.Assists}",
                        DamageLabel = $"{ApiData?.Included[i]?.Attributes?.Stats?.WinPlace}",
                        WalkDistance = $"{newDistance}m",


                        //Name1 = $"{ApiData?.Included[i]?.Relationships?.Team}",
                        //Name2 = $"{ApiData?.Included[i]?.Relationships?.Team}",
                        /*Name1 = "name1",
                        Name2 = "name2",
                        Name3 = "name3",
                        Name4 = "reallylongname",*/
                        
                        //labelStyle = labelR,
                        //grayStack = labelS,


                        MatchDate = $"{ApiData?.Data?.Attributes?.CreatedAt.LocalDateTime}",
                        WonGame = $"{ApiData?.Included[i]?.Attributes?.Won.HasValue}",
                        KillPlace = $"#{ApiData?.Included[i]?.Attributes?.Stats?.KillPlace}",
                        DBNO = $"{ApiData?.Included[i]?.Attributes?.Stats?.DbnOs}",
                        HeadshotKills = $"{ApiData?.Included[i]?.Attributes?.Stats?.HeadshotKills}",
                        LongestKill = $"{ApiData?.Included[i]?.Attributes?.Stats?.LongestKill}",
                        Heals = $"{ApiData?.Included[i]?.Attributes?.Stats?.Heals}",
                        Revives = $"{ApiData?.Included[i]?.Attributes?.Stats?.Revives}",
                        Boosts = $"{ApiData?.Included[i]?.Attributes?.Stats?.Boosts}",
						RideDistance = $"{newRideDistance}m"


                    };

                    //Debug.WriteLine($"{ApiData?.Data?.Relationships?.Rosters?.Data[0]?.Id}");
                    //Debug.WriteLine($"{ApiData?.Data?.Relationships?.Rosters?.Data[1]?.Id}");
                    //Debug.WriteLine($"{ApiData?.Included[i]?.Relationships?.Participants?.Data[0]?.Id}");
                    //Debug.WriteLine($"{ApiData?.Included[i]?.Relationships?.Participants?.Data[1]?.Id}");
                    /* 
                     for (int count = 0; count < ApiData?.Included[i]?.Relationships?.Team?.Data.Length; count++)
                     {
                         //Debug.WriteLine($"{ApiData?.Data?.Relationships?.Rosters?.Data[count]?.Id}");
                         Debug.WriteLine($"{ApiData?.Included[i]?.Relationships?.}");
                     }*/


                    MatchInfo.Add(newList);
                    MatchListView.ItemsSource = MatchInfo;

                    /*
                    for (int i = 0; i <= 100; i++)
                    {
                    //Debug.WriteLine($"{ApiData?.Included[i]?.Attributes?.Stats?.Name}");
                      if (ApiData?.Included[i]?.Attributes?.Stats?.Name == App.userName)
                        {
                            label4.Text = $"Name: {ApiData?.Included[i]?.Attributes?.Stats?.Name}";
                            //label4.TextColor = Color.Blue;
                            label5.Text = $"Placement: {ApiData?.Included[i]?.Attributes?.Stats?.WinPlace}";
                            //label5.TextColor = Color.Blue;
                            label6.Text = $"Damage Dealt: {ApiData?.Included[i]?.Attributes?.Stats?.DamageDealt}";
                            label7.Text = $"Kill Place: {ApiData?.Included[i]?.Attributes?.Stats?.KillPlace}";
                            label8.Text = $"Kills: {ApiData?.Included[i]?.Attributes?.Stats?.Kills}";
                            /*label9.Text = $"Assists: {ApiData?.Included[i]?.Attributes?.Stats?.Assists}";
                            label10.Text = $"Kill Streak: {ApiData.Included[i].Attributes.Stats.KillStreaks}";
                            label11.Text = $"Longest Kill: {ApiData?.Included[i]?.Attributes?.Stats?.LongestKill}";
                            label12.Text = $"Walk Distance: {ApiData?.Included[i]?.Attributes?.Stats?.WalkDistance}";
                            label13.Text = $"Rank: {ApiData?.Included[i].Attributes?.Stats?.WeaponsAcquired}";
                        }


                    }*/

                } //response true if

            } //end of while for match id's
        }
    }
}