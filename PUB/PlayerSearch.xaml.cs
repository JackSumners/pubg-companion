﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace PUB
{
    public partial class PlayerSearch : ContentPage
    {
        public PlayerSearch()
        {
            InitializeComponent();
            

			CheckConnection();

			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await DisplayAlert("Warning", "Internet connection lost! Please reconnect your device while we send you to the home page!", "OK");

                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }
            };
        }

		Boolean CheckConnection()
		{
			if (CrossConnectivity.Current.IsConnected == true)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
        {
			if (CheckConnection())
			{
				//cases will be if we find a player
				if (usernameEntry.Text == null || usernameEntry.Text == "")
				{
					messageOutput.Text = "Error: Please try a valid input!";
				}
				else
				{
					App.userName = usernameEntry.Text;
					await Navigation.PushAsync(new PUBPage());
				}
			}
			else
            {
				await DisplayAlert("Issue", "Cannot search without connection!", "OK");
            }
        }
    }
}
