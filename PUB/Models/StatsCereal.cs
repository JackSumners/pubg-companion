﻿namespace PUB.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class StatsCereal
    {
        [JsonProperty("data")]
        public StatsCerealDatum[] Data { get; set; }

        [JsonProperty("links")]
        public StatsCerealLinks Links { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }
    }

    public partial class StatsCerealDatum
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }

        [JsonProperty("relationships")]
        public Relationships Relationships { get; set; }

        [JsonProperty("links")]
        public DatumLinks Links { get; set; }
    }

    public partial class Attributes
    {
        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("patchVersion")]
        public string PatchVersion { get; set; }

        [JsonProperty("shardId")]
        public string ShardId { get; set; }

        [JsonProperty("stats")]
        public object Stats { get; set; }

        [JsonProperty("titleId")]
        public string TitleId { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }
    }

    public partial class DatumLinks
    {
        [JsonProperty("schema")]
        public string Schema { get; set; }

        [JsonProperty("self")]
        public string Self { get; set; }
    }

    public partial class Relationships
    {
        [JsonProperty("assets")]
        public Assets Assets { get; set; }

        [JsonProperty("matches")]
        public Assets Matches { get; set; }
    }

    public partial class Assets
    {
        [JsonProperty("data")]
        public AssetsDatum[] Data { get; set; }
    }

    public partial class AssetsDatum
    {
        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class StatsCerealLinks
    {
        [JsonProperty("self")]
        public string Self { get; set; }
    }

    public partial class Meta
    {
    }

    public enum TypeEnum { Match };

    public partial class StatsCereal
    {
        public static StatsCereal FromJson(string json) => JsonConvert.DeserializeObject<StatsCereal>(json, PUB.Models.Converter.Settings);
    }

    static class TypeEnumExtensions
    {
        public static TypeEnum? ValueForString(string str)
        {
            switch (str)
            {
                case "match": return TypeEnum.Match;
                default: return null;
            }
        }

        public static TypeEnum ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this TypeEnum value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case TypeEnum.Match: serializer.Serialize(writer, "match"); break;
            }
        }
    }

    public static class Serialize
    {
        public static string ToJson(this StatsCereal self) => JsonConvert.SerializeObject(self, PUB.Models.Converter.Settings);
    }

    internal class Converter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(TypeEnum))
                return TypeEnumExtensions.ReadJson(reader, serializer);
            if (t == typeof(TypeEnum?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return TypeEnumExtensions.ReadJson(reader, serializer);
            }
            throw new Exception("Unknown type");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
            if (t == typeof(TypeEnum))
            {
                ((TypeEnum)value).WriteJson(writer, serializer);
                return;
            }
            throw new Exception("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new Converter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
