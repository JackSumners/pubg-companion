﻿using Plugin.Connectivity;
using Xamarin.Forms;

namespace PUB
{
    public partial class PUBPage : TabbedPage
    {
        public PUBPage()
        { 
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await Navigation.PopToRootAsync();
                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }
            };
        }
    }
}
