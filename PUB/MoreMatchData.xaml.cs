﻿using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using PUB.Models2;
using Xamarin.Forms;

namespace PUB
{
    public partial class MoreMatchData : ContentPage
    {
        public MoreMatchData(MatchListBinding matchListData)
        {
            InitializeComponent();

			CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
				if (!args.IsConnected)
                {
                    await DisplayAlert("Warning", "Internet connection lost! Please reconnect your device while we send you to the home page!", "OK");

                }
                else
                {
                    await DisplayAlert("", "Regained connection", "OK");
                }
            };

			var searchItem = new ToolbarItem()
            {
                Text = "New Search",
                Command = new Command(() => Navigation.PopToRootAsync()),
            };
            ToolbarItems.Add(searchItem);


            BindingContext = matchListData;         
        }
    }
}

