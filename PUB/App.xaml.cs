﻿using Xamarin.Forms;

namespace PUB
{
    public partial class App : Application
    {
        public static string userName;

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new PlayerSearch());
        }

        protected override void OnStart()
        {
                
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
